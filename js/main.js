const inputsCheckbox = document.querySelectorAll('.container-custom-checkbox input'),
    ingredients = document.querySelectorAll('.current-pizza-item'),
    drinkItems = document.querySelectorAll('.select-drink-item'),
    totalAmount = document.querySelector('.total-amount>.summa'),
    orderBtn = document.querySelector('.typical-btn'),
    modalWindow = document.querySelector('.modal-window'),
    submitBtn = document.querySelector('.modal-window__submit-btn');

const subject = document.querySelector('.modal-window__subject'),
    ingredientsSpan = document.querySelector('.modal-window__ingredients'),
    drinksSpan = document.querySelector('.modal-window__drinks');

const addIngredients = checkboxes => {
    const nodesArray = Array.from(checkboxes);
    const ingredientsArray = Array.from(ingredients);
    ingredientsArray.splice(0, 2);

    for(let checkbox of checkboxes) {
        checkbox.addEventListener('click', event => {
            event.target.parentNode.classList.toggle('active');
            const index = nodesArray.indexOf(event.target);
            ingredientsArray[index].classList.toggle('active');
            calculateOrder();
        })
    }
};

addIngredients(inputsCheckbox);

const addDrinks = drinks => {
    for(let drink of drinks) {
        drink.addEventListener('click', event => {
            event.target.parentNode.classList.toggle('active');
            calculateOrder();
        })
    }
};

addDrinks(drinkItems);

const calculateOrder = () => {
    const addedIngredients = document.querySelectorAll('.container-custom-checkbox.active'),
        addedDrinks = document.querySelectorAll('.select-drink-item.active');

    const startPrice = 300,
        ingredientsPrice = addedIngredients.length * 25,
        drinksPrice = addedDrinks.length * 95;

    totalAmount.innerHTML = `${startPrice + ingredientsPrice + drinksPrice}₽`;
};

window.addEventListener('click', event => {
    if(event.target === modalWindow) {
        modalWindow.classList.add('none');
    }
});

submitBtn.addEventListener('click', () => {
    modalWindow.classList.add('none');
});

const prepareModalWindowContent = () => {
    subject.innerHTML = '';
    ingredientsSpan.innerHTML = '';
    drinksSpan.innerHTML = '';

    const addedIngredients = document.querySelectorAll('.container-custom-checkbox.active'),
        addedDrinks = document.querySelectorAll('.select-drink-item.active');

    let ingredientsList = [];
    if(addedIngredients) {
        for(let ingredient of addedIngredients) {
            ingredientsList.push(ingredient.innerText);
        }
    };

    let drinksList = [];
    if(addedDrinks) {
        for(let drink of addedDrinks) {
            drinksList.push(drink.dataset.name);
        }
    };

    const totalIngredients = ingredientsList.join(', ') || 'нет ингредиентов';
    const totalDrinks = drinksList.join(', ') || 'нет напитков';
    const totalText = `Вы заказали пиццу с ингредиентами: '${totalIngredients}', а также напитки: '${totalDrinks}', с Вас ${totalAmount.innerHTML}`;

    subject.innerHTML = totalText;
}

orderBtn.addEventListener('click', () => {
    modalWindow.classList.remove('none');
    prepareModalWindowContent();
});